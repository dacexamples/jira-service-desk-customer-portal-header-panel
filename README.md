# JIRA Service Desk - Customer Portal - Header Panel add-on example.

Welcome to this add-on example.

This add-on example shows you how you can add a Header Panel to the JIRA Service Desk Customer Portal.

We are using the ACE ([Atlassian Connect Express](https://bitbucket.org/atlassian/atlassian-connect-express)) framework. 

## Want to know more about Header Panels in the JIRA Service Desk Customer Portal?

[Adding your Header Panel to the JIRA Service Desk Customer Portal](https://developer.atlassian.com/jiracloud/jira-service-desk-modules-customer-portal-39988271.html#JIRAServiceDeskmodules-Customerportal-Headerpanel).